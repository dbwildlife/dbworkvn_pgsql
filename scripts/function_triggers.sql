
--
-- Déclencheurs et fonctions
--

/* 
 * FONCTION ET TRIGGER DE SUIVI DES MODIFICATIONS
 * ===============================================================
 * Création d'une fonction et de déclencheurs pour la mise à jour 
 * auto du champ "timestamp_update" permettant de tracer l'historique des mise à jour */
CREATE OR REPLACE FUNCTION update_table() RETURNS TRIGGER
LANGUAGE plpgsql
AS
$$
BEGIN
    IF (NEW != OLD) THEN
        NEW.timestamp_update = CURRENT_TIMESTAMP;
        NEW.updater_user = CURRENT_USER;
        RETURN NEW;
    END IF;
    RETURN OLD;
END;
$$;

-- Création d'une fonction et des déclencheurs pour la mise à jour auto du champ "timestamp_update" permettant de tracer l'historique des mise à jour
CREATE TRIGGER trig_update_zone_polygon
BEFORE UPDATE ON studyzone.zone_polygon
FOR EACH ROW EXECUTE PROCEDURE update_table();

