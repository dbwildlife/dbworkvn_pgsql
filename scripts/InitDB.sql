/* ******************************************************* *
 * SCHEMAS DE LA BASE DE DONNEE							   *
 * ========================================================*
 * Création des schémas de la base de donnée d'exploitaion *
 * ******************************************************* */
CREATE
	SCHEMA IF NOT EXISTS status AUTHORIZATION lpo07;

CREATE
	SCHEMA IF NOT EXISTS repos AUTHORIZATION lpo07;

CREATE
	SCHEMA IF NOT EXISTS boundary AUTHORIZATION lpo07;

CREATE
	SCHEMA IF NOT EXISTS naturalarea AUTHORIZATION lpo07;

CREATE
	SCHEMA IF NOT EXISTS dict AUTHORIZATION lpo07;

CREATE
	SCHEMA IF NOT EXISTS fdw AUTHORIZATION lpo07;

CREATE
	SCHEMA IF NOT EXISTS landuse AUTHORIZATION lpo07;

CREATE
	SCHEMA IF NOT EXISTS studyzone AUTHORIZATION lpo07;

/* ********************************************************************** *
 * TERRITOIRE D'AGGREMENT												  *
 * ====================================================================== *
 * Création d'une table représentant le territoire de la zone d'étude     *
 * Cette table conditionnera l'ensemble des imports de données ultérieurs *
 * afin de n'intégrer de base que les données concernant le territoire    *
 * d'agrément de l'association                                            *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * /!\ Ne pas oublier d'intégrer à cette table le territoire d'aggrément  *
 * de l'apn pour importer les données issues du projet dbgeneraldata      *
 * ********************************************************************** */
CREATE
	TABLE
		repos.apn_territory(
			id SERIAL PRIMARY KEY NOT NULL,
			NAME VARCHAR(100) NOT NULL UNIQUE,
			geom geometry(
				multipolygon,
				2154
			)
		);

/* **************************************************************** *
 * DICTIONNAIRES									   				*
 * ================================================================	*
 * Création des tables du schéma dict regroupant les dictionnaires  *
 * de correspondances des valeurs des champs et des statuts des		* 
 * espèces															*
 * **************************************************************** */
/* **************************************************************** *
 * Espèces de Biolovision 							   				*
 * **************************************************************** */
CREATE
	TABLE
		dict.biolovision_species(
			id INTEGER PRIMARY KEY,
			id_taxo_group INTEGER,
			sysname_taxo_group VARCHAR(30),
			french_name VARCHAR(100),
			latin_name VARCHAR(100),
			sys_order INTEGER,
			id_family INTEGER,
			sysname_family_name VARCHAR(50)
		) ALTER TABLE
			dict.biolovision_species owner TO lpo07;

COMMENT ON
TABLE
	dict.biolovision_species IS 'Liste complète des espèces Biolovision';

/* **************************************
 * Création d'une table de zone d'étude *
 ****************************************/
--DROP TABLE IF EXISTS studyzone.zone_polygon CASCADE;
CREATE
	TABLE
		studyzone.zone_polygon(
			id SERIAL NOT NULL PRIMARY KEY,
			project_ref VARCHAR(50) NULL,
			project_name VARCHAR(255) NULL,
			buffer_dist NUMERIC(
				10,
				2
			) NULL,
			zone_name VARCHAR(128) NOT NULL,
			geom geometry(
				multipolygon,
				2154
			) NOT NULL,
			timestamp_insert TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
			timestamp_update TIMESTAMP WITH TIME ZONE,
			creator_user VARCHAR(50) DEFAULT CURRENT_USER,
			updater_user VARCHAR(50)
		);

CREATE
	INDEX zone_polygon_gidx ON
	studyzone.zone_polygon(
		geom DESC
	);

CREATE
	INDEX zone_polygon_idx ON
	studyzone.zone_polygon(
		id DESC
	);

/*
 * Création d'un dictionnaire des correspondances des codes atlas
 * */
DROP
	TABLE
		IF EXISTS dict.atlas_code;

CREATE
	TABLE
		dict.atlas_code(
			atlas_code INTEGER UNIQUE NOT NULL PRIMARY KEY,
			atlas_descr VARCHAR(255) NOT NULL,
			breeding_code INTEGER NOT NULL,
			breeding_descr VARCHAR(100) NOT NULL
		);

INSERT
	INTO
		dict.atlas_code
	VALUES(
		0,
		'Absence de code',
		0,
		'Absence de code'
	),
	(
		1,
		'Code non valide',
		2,
		'Code non valide'
	),
	(
		2,
		'Présence dans son habitat durant sapériode de nidification',
		12,
		'Nicheur possible'
	),
	(
		3,
		'Mâle chanteur présent en période denidification',
		12,
		'Nicheur possible'
	),
	(
		4,
		'Couple présent dans son habitatdurant sa période de nidification',
		13,
		'Nicheur probable'
	),
	(
		5,
		'Comportement territorial (chant,querelles avec des voisins, etc.) observé sur un même territoire',
		13,
		'Nicheur probable'
	),
	(
		6,
		'Comportement nuptial: parades,copulation ou échange de nourritureentre adultes',
		13,
		'Nicheur probable'
	),
	(
		7,
		'Visite d''un site de nidificationprobable. Distinct d''un site de repos',
		13,
		'Nicheur probable'
	),
	(
		8,
		'Cri d''alarme ou tout autrecomportement agité indiquant laprésence d''un nid ou de jeunes auxalentours',
		13,
		'Nicheur probable'
	),
	(
		9,
		'Preuve physiologique: plaqueincubatrice très vascularisée ou œufprésent dans l''oviducte. Observationsur un oiseau en main',
		13,
		'Nicheur probable'
	),
	(
		10,
		'Transport de matériel ou constructiond''un nid; forage d''une cavité (pics)',
		13,
		'Nicheur probable'
	),
	(
		11,
		'Oiseau simulant une blessure oudétournant l''attention, tels lescanards, gallinacés, oiseaux de rivage,etc.',
		14,
		'Nicheur certain'
	),
	(
		12,
		'Nid vide ayant été utilisé ou coquillesd''œufs de la présente saison.',
		14,
		'Nicheur certain'
	),
	(
		13,
		'Jeunes en duvet ou jeunes venant dequitter le nid et incapables de soutenirle vol sur de longues distances',
		14,
		'Nicheur certain'
	),
	(
		14,
		'Adulte gagnant, occupant ou quittantle site d''un nid; comportementrévélateur d''un nid occupé dont lecontenu ne peut être vérifié (trop hautou dans une cavité)',
		14,
		'Nicheur certain'
	),
	(
		30,
		'Nicheur possible',
		12,
		'Nicheur possible'
	),
	(
		15,
		'Adulte transportant un sac fécal',
		14,
		'Nicheur certain'
	),
	(
		16,
		'Adulte transportant de la nourriturepour les jeunes durant sa période denidification',
		14,
		'Nicheur certain'
	),
	(
		17,
		'Coquilles d''œufs éclos',
		14,
		'Nicheur certain'
	),
	(
		18,
		'Nid vu avec un adulte couvant',
		14,
		'Nicheur certain'
	),
	(
		19,
		'Nid contenant des œufs ou des jeunes(vus ou entendus)',
		14,
		'Nicheur certain'
	),
	(
		40,
		'Nidification probable',
		13,
		'Nicheur probable'
	),
	(
		50,
		'Nidification certaine',
		14,
		'Nicheur certain'
	),
	(
		99,
		'Espèce absente malgré des recherches',
		1,
		'Espèce absente'
	),
	(
		100,
		'Espèce absente malgré des recherches',
		1,
		'Espèce absente'
	);

CREATE
	INDEX atlas_code_idx ON
	dict.atlas_code(
		id DESC;
